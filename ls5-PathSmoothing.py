#! /usr/bin/env python
# -----------
# User Instructions
#
# Define a function smooth that takes a path as its input
# (with optional parameters for weight_data, weight_smooth,
# and tolerance) and returns a smooth path. The first and
# last points should remain unchanged.
#
# Smoothing should be implemented by iteratively updating
# each entry in newpath until some desired level of accuracy
# is reached. The update should be done according to the
# gradient descent equations given in the instructor's note
# below (the equations given in the video are not quite
# correct).
# -----------

from copy import deepcopy
from math import sqrt

# thank you to EnTerr for posting this on our discussion forum
def printpaths(path,newpath):
  for old,new in zip(path,newpath):
    print '['+ ', '.join('%.3f'%x for x in old) + \
      '] -> ['+ ', '.join('%.3f'%x for x in new) +']'

# Don't modify path inside your function.
path = [[0, 0],
        [0, 1],
        [0, 2],
        [1, 2],
        [2, 2],
        [3, 2],
        [4, 2],
        [4, 3],
        [4, 4]]

def smooth(path, weight_data = 0.5, weight_smooth = 0.1, tolerance = 0.000001):
  
  # Make a deep copy of path into newpath
  newpath = deepcopy(path)
  
  #######################
  ### ENTER CODE HERE ###
  #######################
  changeDelta = 999.0
  iterations = 0
  while (changeDelta >= tolerance):
    changeDelta = 0;
    for i in range (1, len(path)-1):
      #looks to get a pointer, changes with newpath[i]
      #tempPoint = newpath[i]
      
      #store actual value, vs a pointer (i think) from above commented out code
      tempX = newpath[i][0]
      tempY = newpath[i][1]
      
      ##optimize distance from original point and next and previsou point
      ##note: both optimizations of original point, and distance to previous and next point should be done
      ##      with reference to the same newpath[] value. therefore should be done in the same equation
      ##      or save a temp values
      # yi <- yi + alpha * (xi - yi) + beta * (yi + 1 + yi - 1 - 2 * yi)
      
      #set x value
      newpath[i][0] = newpath[i][0] + weight_data*(path[i][0] - newpath[i][0]) + weight_smooth*(newpath[i-1][0] + newpath[i+1][0] - 2*newpath[i][0])
      
      #set y value
      newpath[i][1] = newpath[i][1] + weight_data*(path[i][1] - newpath[i][1]) + weight_smooth*(newpath[i-1][1] + newpath[i+1][1] - 2*newpath[i][1])
      
      #dum tolerance check, accumulative
      changeDelta += abs(tempX - newpath[i][0]) + abs(tempY - newpath[i][1])
      #print "iterations, changeDelta, x delta, y delta", iterations, changeDelta, tempX - newpath[i][0], tempY - newpath[i][1]
      
      #print "eucldn dist: pointA, pointB, dist --> ", newpath[i], tempPoint, changeDelta
    iterations += 1

  print "----", iterations
  
  return newpath # Leave this line for the grader!


printpaths(path,smooth(path))