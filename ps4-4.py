#! /usr/bin/env python
# --------------
# USER INSTRUCTIONS
#
# Write a function called stochastic_value that
# returns two grids. The first grid, value, should
# contain the computed value of each cell as shown
# in the video. The second grid, policy, should
# contain the optimum policy for each cell.
#
# --------------
# GRADING NOTES
#
# We will be calling your stochastic_value function
# with several different grids and different values
# of success_prob, collision_cost, and cost_step.
# In order to be marked correct, your function must
# RETURN (it does not have to print) two grids,
# value and policy.
#
# When grading your value grid, we will compare the
# value of each cell with the true value according
# to this model. If your answer for each cell
# is sufficiently close to the correct answer
# (within 0.001), you will be marked as correct.

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>'] # Use these when creating your policy grid.

# ---------------------------------------------
#  Modify the function stochastic_value below
# ---------------------------------------------
#pass in the cell moving too
def cost_move(x, y, percentage, values):
    if (x) >= len(grid[0]) or (x) < 0 or (y) >= len(grid) or (y) < 0 or grid[x][y] == 1:
        #collision
        return percentage * collision_cost
    else:
        # no collision
        return percentage * values[x][y]

def stochastic_value(grid,goal,cost_step,collision_cost,success_prob):
    failure_prob = (1.0 - success_prob)/2.0 # Probability(stepping left) = prob(stepping right) = failure_prob
    value = [[1000 for row in range(len(grid[0]))] for col in range(len(grid))]
    policy = [[' ' for row in range(len(grid[0]))] for col in range(len(grid))]
    
    #set goal cost to 0
    g_x = goal[0]
    g_y = goal[1]
    size_x = len(grid) # number of rows
    size_y = len(grid[0]) # number of cols
    failure_prob_half = (1.0 - success_prob)/2.0
    value[g_x][g_y] = goal_value
    policy[g_x][g_y]='*'
    
    #brute force is check all neighbors for lower cost
    #repeast until no changes
    values_changed = 1
    iterations = 1
    while (values_changed and iterations):
        values_changed = 0
        iterations += 1
        for i in range(size_x):
            for j in range(size_y):
                #is it a valid cell?
                if (grid[i][j] == 0):
                    #check all neighbors
                    for k in range(len(delta)):
                        #check all three possibilites of the move
                        #correct movement
                        x_inc = delta[k][0] + i
                        y_inc = delta[k][1] + j
                        #tempValue = cost_move(x_inc, y_inc, success_prob, value)
                        tempValue = 0
                        if (x_inc) >= size_x or (x_inc) < 0 or (y_inc) >= size_y or (y_inc) < 0 or grid[x_inc][y_inc] == 1:
                            #collision
                            tempValue += success_prob * collision_cost
                        else:
                            # no collision
                            tempValue += success_prob * value[x_inc][y_inc]
                        
                        
                        
                        #incorrect -1
                        delta_index = (k-1) % len(delta)
                        x_inc = delta[delta_index][0] + i
                        y_inc = delta[delta_index][1] + j
                        #tempValue += cost_move(x_inc, y_inc, failure_prob_half, value)
                        if (x_inc) >= size_x or (x_inc) < 0 or (y_inc) >= size_y or (y_inc) < 0 or grid[x_inc][y_inc] == 1:
                            #collision
                            tempValue += failure_prob_half * collision_cost
                        else:
                            # no collision
                            tempValue += failure_prob_half * value[x_inc][y_inc]
                        
                        #incorrect +1
                        delta_index = (k+1) % len(delta)
                        x_inc = delta[delta_index][0] + i
                        y_inc = delta[delta_index][1] + j
                        #tempValue += cost_move(x_inc, y_inc, failure_prob_half, value)
                        if (x_inc) >= size_x or (x_inc) < 0 or (y_inc) >= size_y or (y_inc) < 0 or grid[x_inc][y_inc] == 1:
                            #collision
                            tempValue += failure_prob_half * collision_cost
                        else:
                            # no collision
                            tempValue += failure_prob_half * value[x_inc][y_inc]
                        
                        
                        if value[i][j] > tempValue + 1:
                            #print "setting [%i,%i] to [%i,%i]", i,j, i+x_inc, j+y_inc
                            value[i][j] = tempValue + 1;
                            policy[i][j] = delta_name[k]
                            values_changed = 1
    
    print "----- ", iterations
    return value, policy


# ---------------------------------------------
#  Use the code below to test your solution
# ---------------------------------------------

####

grid = [[0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 1, 1, 0]]
goal = [0, 3]
cost_step = 1
collision_cost = 100
success_prob = 0.5
goal_value = 0



value,policy = stochastic_value(grid,goal,cost_step,collision_cost,success_prob)
for row in value:
    print row
for row in policy:
    print row

# Expected outputs:
#
# [57.9029, 40.2784, 26.0665,  0.0000]
# [47.0547, 36.5722, 29.9937, 27.2698]
# [53.1715, 42.0228, 37.7755, 45.0916]
# [77.5858, 1000.00, 1000.00, 73.5458]
#
# ['>', 'v', 'v', '*']
# ['>', '>', '^', '<']
# ['>', '^', '^', '<']
# ['^', ' ', ' ', '^']

#admissible heuristic for a-star.
#   reduce 3d problem to 2d and solve for goal, use as first heuristic
#   use 3d without obstacles and solve to goal, use as second heuristic
#   this gianed a 10^100 gains
