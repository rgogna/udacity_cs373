#! /usr/bin/env python
colors = [['red', 'green', 'green', 'red' , 'red'],
          ['red', 'red', 'green', 'red', 'red'],
          ['red', 'red', 'green', 'green', 'red'],
          ['red', 'red', 'red', 'red', 'red']]

colorsTest = [['green', 'green', 'green'],
          ['green', 'red',   'red'],
          ['green', 'green', 'green']]

measurementsTest = ['red', 'red']
           
measurements = ['green', 'green', 'green' ,'green', 'green']


motions = [[0,0],[0,1],[1,0],[1,0],[0,1]]
motionsTest = [[0,0], [0,1]]
           
sensor_right = 0.7

p_move = 0.8

def show(p):
    for i in range(len(p)):
        print p[i]

#DO NOT USE IMPORT
#ENTER CODE BELOW HERE
#ANY CODE ABOVE WILL CAUSE
#HOMEWORK TO BE GRADED
#INCORRECT

#find even probability distribution
numOfRows = len(colors)
numOfCols = len(colors[0])
evenProb = 1.0 / (numOfRows + numOfCols)

#Function for move, allow up, down, left, right; circular world
#motion is a [up_down, left_right] array
def move(p, motion):
    sum = 0.0
    q = []
    for i in range(numOfRows):
        q.append([]) #append another row
        for j in range (numOfCols):
            # p(x[i,j] | motion = failed) = p(x[i,j])p(motion=failed | x[i,j])
            temp = (1 - p_move) * p[i][j]
            
            originCell_row = (i - motion[0]) % numOfRows
            originCell_col = (j - motion[1]) % numOfCols
            
            # += p(x[i,j] | motion=success) = p(x[i_origin,j_origin])p(motion=sucess | x[i_origin,j_origin])
            temp = temp + (p_move * p[originCell_row][originCell_col])
            q[i].append(temp) #append another column
            sum += temp
    q = normalize(q, 1.0/sum)
    return q


def sense(p, color):
    sum = 0.0
    q = []
    for i in range (numOfRows):
        q.append([]) #append a row
        for j in range (numOfCols):
            if color == colors[i][j]:
                #if colors match
                #p(x[i, j] | sense = true) = p(x[i,j])p(sense = true | map [i, j])
                q[i].append (p[i][j] * sensor_right)
            else:
                #if colors dont match
                #p(x[i, j] | sense = false) = p(x[i,j])p(sense = false | map [i, j])
                q[i].append (p[i][j] * (1 - sensor_right))
            sum += q[i][j]
    q = normalize (q, 1.0/sum)
    #q = normalize (q, sum)
    return q

#getting a rounding error
def normalize(p, factor):
    #sum = 0.0
    for i in range (numOfRows):
        for j in range (numOfCols):
            p[i][j] = p[i][j] * factor
            #sum += p[i][j]
    #print (sum)
    return p

#set probablity 2d array to evenProb
p = [[evenProb for i in range(numOfCols)]*1 for i in range(numOfRows)]

for i in range(len(measurements)):
    p = move(p, motions[i])
    p = sense(p, measurements[i])


#Your probability array must be printed
#with the following code.
show(p)