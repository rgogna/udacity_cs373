#! /usr/bin/env python
# ----------
# User Instructions:
#
# Define a function, search() that takes no input
# and returns a list
# in the form of [optimal path length, x, y]. For
# the grid shown below, your function should output
# [11, 4, 5].
#
# If there is no valid path from the start point
# to the goal, your function should return the string
# 'fail'
# ----------

# Grid format:
#   0 = Navigable space
#   1 = Occupied space

grid = [[0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 1, 0],
        [0, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 0]]

init = [0, 0]
goal = [len(grid)-1, len(grid[0])-1] # Make sure that the goal definition stays in the function.

delta = [[-1, 0 ], # go up
         [ 0, -1], # go left
         [ 1, 0 ], # go down
         [ 0, 1 ]] # go right

delta_name = ['^', '<', 'v', '>']

cost = 1

def search():
    # ----------------------------------------
    # insert code here and make sure it returns the appropriate result
    # ----------------------------------------
    #closed list matching index of grid, 0 is open, 1 is closed
    closed = [[0 for ro in range(len(grid[0]))] for col in range(len(grid))]
    
    #set starting location as closed
    closed [init[0]][init[1]] = 1
    
    x = init[0]
    y = init[1]
    g = 0 #cost to get here
    
    open = [[g, x, y]]
    
    found = False #found goual
    resign = False #no goal is found
    
    while found is False and resign is False:
        if len(open) == 0:
            #no more nodes to expand in the open list
            resign = True
            print 'fail'
            path = 'fail'
        else:
            #find lowest g value
            open.sort() #will sort on first element of the list ie g
            open.reverse() #sorts in accending order smallest up, flip to largerst to smallest
            next = open.pop() #pop the smallest value
            
            g = next[0] #get g, cost of node going to expand
            x = next[1] #get x value
            y = next[2] #get y value
            
            #check if node is the goal!
            if x == goal[0] and y == goal[1]:
                found = True
                print next #print the goal
                path = next
            
            else:
                for i in range(len(delta)):
                    #for all possible movements
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]
                    if x2 >= 0 and x2 < len(grid) and y2 >= 0 and y2 < len(grid[0]):
                        if closed[x2][y2] == 0 and grid[x2][y2] == 0:
                            #not on closed list and it is not an obstacle
                            g2 = g + cost
                            open.append([g2, x2, y2])
                            closed[x2][y2] = 1 #set closed so its not expanded again
    
    
    return path # you should RETURN your result

ans = search()
print ans